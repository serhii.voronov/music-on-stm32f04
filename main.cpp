#include "mbed.h"
#include "pitches.h"

#define NOTE_TIME 1000.0f

PwmOut pwm_out(PB_1);


int Pirates_note[] = {
NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4,  NOTE_D4,  NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_D4,
NOTE_D4, NOTE_D4, NOTE_D4, NOTE_A3, NOTE_C3,  NOTE_D4,  NOTE_D4, NOTE_D4, NOTE_E4, NOTE_F4, NOTE_F4, NOTE_F4, NOTE_G4, NOTE_E4, NOTE_E4, NOTE_D4, NOTE_C3, NOTE_C3, NOTE_D4, 0,       NOTE_A3, NOTE_C3,
NOTE_D4, NOTE_D4, NOTE_D4, NOTE_E4, NOTE_F4,  NOTE_F4,  NOTE_F4, NOTE_G4, NOTE_E4, NOTE_E4, NOTE_D4, NOTE_C3, NOTE_D4, 0,       NOTE_A3, NOTE_C3, NOTE_D4, NOTE_D4, NOTE_D4, NOTE_F4,
NOTE_G4, NOTE_G4, NOTE_G4, NOTE_A4, NOTE_AS4, NOTE_AS4, NOTE_A4, NOTE_G4, NOTE_A4, NOTE_D4, 0,       NOTE_D4, NOTE_E4, NOTE_F4, NOTE_F4, NOTE_G4, NOTE_A4, NOTE_D4, 0,       NOTE_D4, NOTE_D4,
NOTE_E4, NOTE_E4, NOTE_F4, NOTE_D4, NOTE_E4,  0,        0,       NOTE_A4, NOTE_C4, NOTE_D5, NOTE_D5, NOTE_D5, NOTE_E5, NOTE_F5, NOTE_F5, NOTE_F5, NOTE_G5, NOTE_E5, NOTE_E5, NOTE_D4, NOTE_C4,
0
};

int Pirates_duration[] = {
4, 8, 4, 8, 4, 8, 8, 8, 8, 4, 8, 4, 8, 4, 8, 8, 8, 8, 4, 8, 4, 8, 
4, 8, 8, 8, 8, 4, 4, 8, 8, 4, 4, 8, 8, 4, 4, 8, 8, 8, 4, 8, 8, 8, 
4, 4, 8, 8, 4, 4, 8, 8, 4, 4, 8, 8, 4, 4, 8, 8, 4, 4, 8, 8,
4, 4, 8, 8, 4, 4, 8, 8, 8, 4, 8, 8, 8, 4, 4, 4, 8, 4, 8, 8, 8, 
4, 4, 8, 8, 8, 4, 8, 8, 8, 4, 4, 8, 8, 4, 4, 8, 8, 4, 4, 8, 8,
0
};

void tone(int fr, int time){
    pwm_out.write(0.5f);
    pwm_out.period(1.0f / fr);
    wait_ms(time);
    pwm_out.write(0.0f);
}

void play_note(int note, int duration){
    if(note == 0){
        wait_ms(NOTE_TIME / duration);  
    }
    else{
        tone(note, NOTE_TIME / duration);
        wait_ms((NOTE_TIME / duration) / 20.0);
    }
}

int main(){
    int i = 0;
    while(Pirates_duration[i] != 0){
        play_note(Pirates_note[i], Pirates_duration[i]);
        i++;
    }
    while(1);
}
